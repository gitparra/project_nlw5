import { Router } from 'express';
import { SettingsController } from './controllers/SettingsController';
import { UsersController } from './controllers/UsersController';
import { MessagesController } from './controllers/MessagesController';

const routes = Router();
const settingsController = new SettingsController();
const usersController = new UsersController();
const messagesController = new MessagesController();

routes.get('/', (req, res) => {
    return res.json({
        message: 'Ola NWL 05',
    });
});

routes.post('/', (req, res) => {
    return res.json({
        message: 'Usuario salvo com sucesso!',
    });
});

routes.post('/settings', settingsController.create);
routes.put('/settings/username/:username', settingsController.update);
routes.get('/settings/username/:username', settingsController.findByUsername);
routes.post('/users', usersController.create);
routes.post('/messages', messagesController.create);
routes.get('/messages/user/:user', messagesController.showByUser);

export { routes };
