import { Request, Response } from 'express';
import { MessagesService } from '../services/MessagesService';

class MessagesController {
    async create(req: Request, res: Response) {
        const messagesService = new MessagesService();

        const {
            body: {
                admin_id,
                text,
                user_id,
            },
        } = req;

        const message = await messagesService.create({
            admin_id,
            text,
            user_id,
        });

        return res.json(message);
    }

    async showByUser(req: Request, res: Response) {
        const messagesService = new MessagesService();

        const { user } = req.params;

        const messagesList = await messagesService.listByUser(user);

        return res.json(messagesList);
    }
}

export { MessagesController };
