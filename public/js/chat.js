let socket_admin_id = null;
let emailUser = null;
let socket = null;

document.querySelector("#start_chat").addEventListener("click", (event) => {
    const chatHelp = document.getElementById('chat_help');
    chatHelp.style.display = 'none';

    const chatInSupport = document.getElementById('chat_in_support');
    chatInSupport.style.display = 'block';

    socket = io();

    const email = document.getElementById('email').value;
    emailUser = email;
    const text = document.getElementById('txt_help').value;

    socket.on('connect', () => {
        const params = {
            email,
            text,
        };

        socket.emit('client_first_access', params, (call, err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(call);
            }
        });
    });

    socket.on('client_list_all_messages', (messages) => {
        var templateClient = document.getElementById('message-user-template').innerHTML;
        var templateAdmin = document.getElementById('admin-template').innerHTML;

        messages.forEach((message) => {
            if (message.admin_id === null) {
                const rendered = Mustache.render(templateClient, {
                    message: message.text,
                    email: message.email,
                });

                document.getElementById('messages').innerHTML += rendered;
            } else {
                const rendered = Mustache.render(templateAdmin, {
                    message_admin: message.text,
                });

                document.getElementById('messages').innerHTML += rendered;
            }
        });
    });

    socket.on('admin_send_to_client', (message) => {
        socket_admin_id = message.socket_id;
        const templateAdmin = document.getElementById('admin-template').innerHTML;

        const rendered = Mustache.render(templateAdmin, {
            message_admin: message.text,
        });

        document.getElementById('messages').innerHTML += rendered;
    });
});

document.querySelector("#send_message_button").addEventListener("click", (event) => {
    const text = document.getElementById("message_user");

    const params = {
        text: text.value,
        socket_admin_id,
    };

    socket.emit("client_send_to_admin", params);

    const templateClient = document.getElementById("message-user-template").innerHTML;

    const rendered = Mustache.render(templateClient, {
        message: text.value,
        email: emailUser,
    });

    document.getElementById("messages").innerHTML += rendered;
    text.value = '';
});
